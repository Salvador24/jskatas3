const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
var destination = document.getElementById("d1");
let textContent = "";
let kataNo = 1;

function createKataBar(content) {
    let newElement = document.createElement("div");
    newElement.className = "bar";


    // Place a text label inside the new div.
    var newText = document.createTextNode(content);
    newElement.appendChild(newText);

    // Put the new div on the page inside the existing element "d1".
    destination.appendChild(newElement);
}

function createNewElement(content) {
    let newElement = document.createElement("div");

    // Place a text label inside the new div.
    var newText = document.createTextNode(content);
    newElement.appendChild(newText);
    // Put the new div on the page inside the existing element "d1".
    destination.appendChild(newElement);
}
// Create a div, with class "bar", and set the width to 100px.

// Kata-1 

createKataBar("Kata 1 - Display the numbers from 1 to 20. ");
textContent = "";
for (let i = 1; i <= 20; i++) {
    textContent = textContent + " " + i;
}
createNewElement(textContent)
// Kata-2   
createKataBar("Kata 2 - Display the even numbers from 1 to 20. ");
textContent = "";
for (let i = 1; i <= 20; i++) {
    if ((i % 2) == 0) {

        textContent = textContent + " " + i;
    }
}
createNewElement(textContent)
//Kata-3
createKataBar("Kata 3 - Display the odd numbers from 1 to 20. ");
textContent = "";
for (let i = 1; i <= 20; i++) {
    if ((i % 2) == 1) {

        textContent = textContent + " " + i;
    }
}
createNewElement(textContent)

//Kata-4
createKataBar("Kata 4 - Display the multiples of 5 up to 100. ");
textContent = "";
for (let i = 5; i <= 100; i++) {
    if ((i % 5) == 0) {

        textContent = textContent + " " + i;
    }
}
createNewElement(textContent)
//Kata-5
createKataBar("Kata 5 - Display the square numbers from 1 up to 100. ");
textContent = "";
for (let i = 1; i * i <= 100; i++) {
    textContent = textContent + " " + i * i;
}
createNewElement(textContent)

//Kata-6
createKataBar("Kata 6 - Display the numbers counting backwards from 20 to 1. ");
textContent = "";
for (let i = 20; i >= 1; i--) {
    textContent = textContent + " " + i;
}
createNewElement(textContent)
//Kata-7
createKataBar("Kata 7 - Display the even numbers counting backwards from 20 to 1. ");
textContent = "";
for (let i = 20; i >= 1; i--) {
    if ((i % 2) == 0) {
        textContent = textContent + " " + i;
    }
}
createNewElement(textContent)

//Kata-8
createKataBar("Kata 8 - Display the odd numbers from 20 to 1, counting backwards. ");
textContent = "";
for (let i = 20; i >= 1; i--) {
    if ((i % 2) == 1) {
        textContent = textContent + " " + i;
    }
}
createNewElement(textContent)

//Kata-9
createKataBar("Kata 9 - Display the multiples of 5, counting down from 100 to 1. ");
textContent = "";
for (let i = 100; i >= 1; i--) {
    if ((i % 5) == 0) {
        textContent = textContent + " " + i;
    }
}
createNewElement(textContent)

//Kata-10
createKataBar("Kata 10 - Display the square numbers, counting down from 100. ");
textContent = "";
for (let i = 100; i * i >= 1; i--) {
    if (i * i <= 100) {
        textContent = textContent + " " + i * i;
    }
}
createNewElement(textContent)

//Kata-11
createKataBar("Kata 11 - Display the 20 elements of sampleArray. ");
createNewElement(sampleArray)

//Kata-12
createKataBar("Kata 12 - Display all the even numbers contained in sampleArray. ");
textContent = "";
for (let i = 0; i <= sampleArray.length; i++) {
    if ((sampleArray[i] % 2) == 0) {
        textContent = textContent + " " + sampleArray[i];
    }
}
createNewElement(textContent)

//Kata-13
createKataBar("Kata 13 - Display all the odd numbers contained in sampleArray. ");
textContent = "";
for (let i = 0; i < sampleArray.length; i++) {
    if ((sampleArray[i] % 2) == 1) {
        textContent = textContent + " " + sampleArray[i];
    }
}
createNewElement(textContent)

//Kata-14
createKataBar("Kata 14 - Display the square of each element in sampleArray. ");
textContent = "";
for (let i = 0; i < sampleArray.length; i++) {

    textContent = textContent + " " + sampleArray[i] * sampleArray[i];

}
createNewElement(textContent)

// Kata-15 
createKataBar("Kata 15 - Display the sum of all the numbers from 1 to 20. ");
textContent = "";
let result = 0;
for (let i = 1; i <= 20; i++) {
    result = result + i
}
textContent = result;
createNewElement(textContent)

// Kata-16 
createKataBar("Kata 16 - Display the sum of all the elements in sampleArray. ");
textContent = "";
result = 0;
for (let i = 0; i < sampleArray.length; i++) {
    result = result + sampleArray[i]
}
textContent = result;
createNewElement(textContent)

// Kata-17
createKataBar("Kata 17 - Display the smallest element in sampleArray. ");
createNewElement(Math.min(...sampleArray))


//Kata-18
createKataBar("Kata 18 - Display the largest element in sampleArray. ");
createNewElement(Math.max(...sampleArray))

//Kata-19
createKataBar("Kata 19 - Display 20 solid gray rectangles, each 20px high and 100px wide. ");
for (let i = 0; i < 20; i++) {
    let otherElement = document.createElement("div");
    otherElement.style.height = "20px";
    otherElement.style.margin = "1px";
    otherElement.style.backgroundColor = "gray";
    otherElement.style.width = "100px";

    //textContent = textContent + " " + i;
    destination.appendChild(otherElement);
}

//Kata-20
createKataBar("Kata 20 - Display 20 solid gray rectangles, each 20px high, with widths ranging evenly from 105px to 200px (remember #4, above). ");
for (let i = 0; i < 20; i++) {
    let otherElement = document.createElement("div");
    otherElement.style.height = "20px";
    otherElement.style.margin = "1px";
    otherElement.style.backgroundColor = "gray";
    otherElement.style.width = 100 + (5 * i) + "px";

    destination.appendChild(otherElement);
}

//Kata-21
createKataBar("Kata 21 - Display 20 solid gray rectangles, each 20px high, with widths in pixels given by the 20 elements of sampleArray. ");
for (let i = 0; i < sampleArray.length; i++) {
    let otherElement = document.createElement("div");
    otherElement.style.height = "20px";
    otherElement.style.margin = "1px";
    otherElement.style.backgroundColor = "gray";
    otherElement.style.width = sampleArray[i] + "px";

    destination.appendChild(otherElement);
}

//Kata-22
createKataBar("Kata 22 - As in #21, but alternate colors so that every other rectangle is red. ");
for (let i = 0; i < sampleArray.length; i++) {
    let otherElement = document.createElement("div");
    otherElement.style.height = "20px";
    otherElement.style.margin = "1px";
    otherElement.style.width = sampleArray[i] + "px";
    if ((i%2)==0){
        otherElement.style.backgroundColor = "gray";   
    }else {
        otherElement.style.backgroundColor = "red";
    }
    destination.appendChild(otherElement);
}

//Kata-23
createKataBar("Kata 23 - As in #21, but color the rectangles with even widths red. ");
for (let i = 0; i < sampleArray.length; i++) {
    let otherElement = document.createElement("div");
    otherElement.style.height = "20px";
    otherElement.style.margin = "1px";
    otherElement.style.width = sampleArray[i] + "px";
    if ((sampleArray[i])%2 ==0){

        otherElement.style.backgroundColor = "red";
    }else{

        otherElement.style.backgroundColor = "gray";
    }


    destination.appendChild(otherElement);
}